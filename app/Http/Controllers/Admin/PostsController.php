<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;


class PostsController extends Controller
{
    public function index(Page $posts){
        $posts = $posts->all();
        return view ('administrator.posts', compact('posts'));
    }
    public function edit(Page $post){
        return view('administrator.editPost',compact('post'));
    }
    public function update(Page $post){
        $this->validate(request(),[
           'title' => 'required',
           'alias' => 'required',
           'intro' => 'required',
           'content' => 'required',
        ]);

        $post->update(request()->all());

        return redirect('/admin/posts');
    }
    public function destroy(Page $post){
        $post->delete();
        return back();
    }
    public function create(){
        return view('administrator.createPost');
    }

    public function store(Page $post){
        $this->validate(request(),[
            'title' => 'required',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required',
        ]);

        Page::create(request(['title', 'alias', 'intro', 'content']));

        return redirect('/admin/posts');
    }


}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;


class OrdersController extends Controller
{
    public function index(Order $orders){
        $orders = $orders->all();
        return view('administrator/order',compact('orders'));
    }

}
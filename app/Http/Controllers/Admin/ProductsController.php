<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductsController extends Controller
{
    public function index(Product $products){
        $products = $products->all();
        return view('administrator/product',compact('products'));
    }
    public function create(){
        return view('administrator/productCreate');
    }
    public function store(){
        $this->validate(request(), [
            'title' => 'required',
            'alias' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        Product::create(request(['title', 'alias', 'description', 'price']));

        return redirect('/admin/products');
    }
    public function edit(Product $product){

        return view('administrator.editProduct',compact('product'));
    }
    public function update(Product $product){
        $this->validate(request(),[
            'title' => 'required',
            'alias' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        $product->update(request()->all());

        return redirect('/admin/products');
    }
    public function destroy(Product $product){
        $product->delete();
        return redirect('/admin/products');
    }
}

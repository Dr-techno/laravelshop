<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;

class PageController extends Controller
{
    public function index(Page $data){
        $pages = $data->all();
        return view ('pages', compact('pages'));
    }
    public function show(Page $page){
        return view ('page', compact('page'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Page;

class ProductController extends Controller
{

    public function index(Product $products){
        $allProducts = $products->all();
        return view('shop')->with(compact('allProducts'));
    }

    public function product(Product $products){
        $pages = Page::all();
        return view('single')->with(compact('products','pages'));
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;

class OrderController extends Controller
{
    public function index(Order $orders){
        $orders = $orders->all();
        return view('administrator.order',compact('orders'));
    }
}

<?php

use Illuminate\Database\Seeder;

class SeederOrdersTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([


            array(
                'customer_name' => 'Alex',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Спасибо все супер'
            ),
            array(
                'customer_name' => 'Соня',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Рекомендую'
            ),
            array(
                'customer_name' => 'Adam',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Отлично! Рекомендую!'
            ),
            array(
                'customer_name' => 'Alex',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Спасибо все супер'
            ),
            array(
                'customer_name' => 'Соня',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Рекомендую'
            ),
            array(
                'customer_name' => 'Adam',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Отлично! Рекомендую!'
            ),
            array(
                'customer_name' => 'Alex',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Спасибо все супер'
            ),
            array(
                'customer_name' => 'Соня',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Рекомендую'
            ),
            array(
                'customer_name' => 'Adam',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Отлично! Рекомендую!'
            ),
            array(
                'customer_name' => 'Alex',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Спасибо все супер'
            ),
            array(
                'customer_name' => 'Соня',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Рекомендую'
            ),
            array(
                'customer_name' => 'Adam',
                'email' => str_random(10).'@gmail.com',
                'phone' => '+38098'.mt_rand(1000000,9999999),
                'feedback' => 'Отлично! Рекомендую!'
            ),
        ]);
    }
}

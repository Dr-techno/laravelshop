<?php

use Illuminate\Database\Seeder;

class SeederProductsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            array(
                'title' => 'Product1',
                'alias' => 'ProductAlias1',
                'price' => '100',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            ),
            array(
                'title' => 'Product2',
                'alias' => 'ProductAlias2',
                'price' => '200',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            ),
            array(
                'title' => 'Product3',
                'alias' => 'ProductAlias3',
                'price' => '300',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            )
        ]);
    }
}

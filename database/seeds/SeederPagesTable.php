<?php

use Illuminate\Database\Seeder;

class SeederPagesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([


            array(
                'title' => 'Text1',
                'alias' => str_random(5),
                'intro' => 'intro '.mt_rand(1,9),
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            ),
            array(
                'title' => 'Text2',
                'alias' => str_random(5),
                'intro' => 'intro '.mt_rand(1,9),
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            ),
            array(
                'title' => 'Text3',
                'alias' => str_random(5),
                'intro' => 'intro '.mt_rand(1,9),
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                In suscipit vehicula purus, vitae egestas odio pellentesque vel. 
                Ut vitae finibus risus. Pellentesque eget vestibulum velit. 
                In rhoncus accumsan sapien non posuere. Nulla pellentesque dapibus augue, 
                quis accumsan est tempus vitae. Nam laoreet odio sem, quis interdum leo 
                faucibus a. Nam risus est, imperdiet quis neque sed, 
                malesuada ullamcorper nibh. Cras molestie ipsum sed malesuada sollicitudin. 
                Ut eu viverra sem, quis aliquet mauris. Phasellus urna sem, malesuada ultricies 
                sollicitudin sollicitudin, venenatis non ex.'
            )
        ]);
    }
}

@extends('administrator.master')
@push('styles')

<!-- DataTables CSS -->
<link href="../css/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../css/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endpush

@push('scripts')

<!-- DataTables JavaScript -->
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
<script src="../js/dataTables.responsive.js"></script>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

@endpush


@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1> Товары</h1>
        </div>

        <div class="panel-body">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Цена</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>



                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                   <tr class="orders">
                        <td class="center">{{$product->id}}</td>
                        <td>{{$product->title}}</td>
                        <td>{{$product->description}}</td>
                        <td>{{$product->price}}</td>
                       <td class="center"><button type="button" class="btn btn-outline btn-primary"><a href="/admin/products/{{$product->alias}}/edit">Редактировать</a></button>

                       </td>
                       <td class="center">
                           <form action="/admin/products/{{$product->alias}}" method="POST">
                               {{csrf_field()}}
                               <input type="hidden" name="_method" value="DELETE">
                               <input type="submit" value="Удалить" class="btn btn-outline btn-danger">
                           </form>

                       </td>

                    </tr>
                @endforeach


                </tbody>
            </table>

        </div>

    </div>




@endsection



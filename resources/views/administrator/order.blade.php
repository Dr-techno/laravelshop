@extends('administrator.master')
@push('styles')

    <!-- DataTables CSS -->
    <link href="../css/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="../css/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endpush

@push('scripts')

<!-- DataTables JavaScript -->
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
<script src="../js/dataTables.responsive.js"></script>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

@endpush


@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">
           <h1> Заказы</h1>
        </div>

        <div class="panel-body">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr>
                    <th>№ Заказа</th>
                    <th>ФИО Заказчика</th>
                    <th>E-mail</th>
                    <th>Телефон</th>
                    <th>Отзыв</th>
                    <th>Создано</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr class="orders">
                    <td class="center">{{$order->id}}</td>
                    <td>{{$order->customer_name}}</td>
                    <td>{{$order->email}}</td>
                    <td>{{$order->phone}}</td>
                    <td>{{$order->feedback}}</td>
                    <td>{{$order->created_at}}</td>
                </tr>
                @endforeach


                </tbody>
            </table>

        </div>

    </div>




@endsection



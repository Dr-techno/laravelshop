@extends('administrator.master')
@push('styles')

<!-- DataTables CSS -->
<link href="../css/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="../css/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endpush

@push('scripts')

<!-- DataTables JavaScript -->
<script src="../js/jquery.dataTables.min.js"></script>
<script src="../js/dataTables.bootstrap.min.js"></script>
<script src="../js/dataTables.responsive.js"></script>


<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>

@endpush


@section('main')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h1> Статьи</h1>
        </div>

        <div class="panel-body">
            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                <thead>
                <tr>
                    <th>Название</th>
                    <th>Алиас</th>
                    <th>Краткое Описание</th>
                    <th>Редактировать</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                @foreach($posts as $post)
                    <tr class="orders">
                        <td>{{$post->title}}</td>
                        <td>{{$post->alias}}</td>
                        <td>{{$post->intro}}</td>
                        <td class="center"><button type="button" class="btn btn-outline btn-primary"><a href="/admin/posts/{{$post->alias}}/edit">Редактировать</a></button>

                        </td>
                        <td class="center">
                            <form action="/admin/posts/{{$post->alias}}" method="POST">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" value="Удалить" class="btn btn-outline btn-danger">
                            </form>

                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>

        </div>

    </div>




@endsection



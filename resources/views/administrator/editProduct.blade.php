@extends('administrator.master')

@section('main')
    @include('other.formErrors')
    <div class="col col-lg-6">
        <form role="form" method="post" action="/admin/products/{{$product->alias}}" >
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label>Название</label>
                <input class="form-control" value="{{$product->title}}" name="title" placeholder="Введите название товара">
            </div>
            <div class="form-group">
                <label>Алиас товара:</label>
                <input class="form-control" value="{{$product->alias}}" name="alias" placeholder="Цена товара">
            </div>
            <div class="form-group">
                <label>Цена товара:</label>
                <input class="form-control" value="{{$product->price}}" name="price" placeholder="Цена товара">
            </div>
            <div class="form-group">
                <label>Описание товара</label>
                <textarea class="form-control"  name="description" rows="3">{{$product->description}}</textarea>
            </div>

            <button type="submit" class="btn btn-default">Редактировать</button>

        </form>

    </div>
@endsection
@extends('administrator.master')

@section('main')
    @include('other.formErrors')
    <div class="col col-lg-6">
        <form role="form" method="post" action="/admin/posts" >
            {{csrf_field()}}

            <div class="form-group">
                <label>Название</label>
                <input class="form-control"  name="title" placeholder="Введите название товара">
            </div>
            <div class="form-group">
                <label>Алиас товара:</label>
                <input class="form-control"  name="alias" placeholder="Цена товара">
            </div>
            <div class="form-group">
                <label>Описание товара</label>
                <textarea class="form-control"  name="intro" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label>Описание товара</label>
                <textarea class="form-control"  name="content" rows="3"></textarea>
            </div>

            <button type="submit" class="btn btn-default">Добавить</button>

        </form>

    </div>
@endsection
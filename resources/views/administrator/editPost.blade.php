@extends('administrator.master')

@section('main')
    @include('other.formErrors')
    <div class="col col-lg-6">
        <form role="form" method="post" action="/admin/posts/{{$post->alias}}" >
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label>Название</label>
                <input class="form-control" value="{{$post->title}}" name="title" placeholder="Введите название товара">
            </div>
            <div class="form-group">
                <label>Алиас товара:</label>
                <input class="form-control" value="{{$post->alias}}" name="alias" placeholder="Цена товара">
            </div>
            <div class="form-group">
                <label>Описание товара</label>
                <textarea class="form-control"  name="intro" rows="3">{{$post->intro}}</textarea>
            </div>
            <div class="form-group">
                <label>Описание товара</label>
                <textarea class="form-control"  name="content" rows="3">{{$post->content}}</textarea>
            </div>

            <button type="submit" class="btn btn-default">Редактировать</button>

        </form>

    </div>
@endsection
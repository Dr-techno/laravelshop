@extends('layouts.master')
@section('main')
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-sidebar col-md-2">
                        <h2 class="sidebar-title">Статьи</h2>
                        <ul>
                            @foreach($pages as $page)
                                <li><a href="./pages/{{$page->alias}}">{{$page->title}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



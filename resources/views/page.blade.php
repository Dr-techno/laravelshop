@extends('layouts.master')
@section('main')
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="single-sidebar col-md-12">
                        <h2 class="sidebar-title">{{$page->title}}</h2>
                        <p>
                            {{$page->content}}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



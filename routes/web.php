<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductController@index');
Route::get('/product/{products}','ProductController@product');




//Делаем дефолтные красивые роуты
Route::resource('pages','PageController');

//Делаем роуты для Заказов
Route::resource('/admin/orders','Admin\OrdersController');

//Делаем роуты для Продуктов
Route::resource('/admin/products','Admin\ProductsController');

//Делаем роуты для Статей
Route::resource('/admin/posts','Admin\PostsController');

//Administrator route
Route::get('/admin','Admin\MainController@index');
//php artisan route:list



